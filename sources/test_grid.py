# 0h h1 Solver. Solves grids of 0h h1 game.
# Copyright (C) 2015  Gabriel Augendre <gabriel@augendre.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'gaugendre'

import pytest
from solver_0hh1 import *


@pytest.fixture
def array():
    return [['B', ' ', ' ', ' '],
            ['B', 'B', ' ', ' '],
            [' ', ' ', ' ', ' '],
            [' ', 'B', ' ', 'B']]


@pytest.fixture
def full_grid(array):
    return Grid(len(array), array)


@pytest.fixture
def empty_grid(array):
    return Grid(len(array))


def test_without_state(empty_grid):
    for line in empty_grid.squares:
        for square in line:
            assert square.state == ' '


def test_with_state(full_grid, array):
    i = 0
    for line in full_grid.squares:
        j = 0
        for square in line:
            assert square.state == array[i][j]
            j += 1
        i += 1