0h h1 solver
============

Solver for the newly released game 0h h1 (see http://0hh1.com/).  
This has been written by Gabriel Augendre and is published under license GPL V3 (see LICENSE).

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂