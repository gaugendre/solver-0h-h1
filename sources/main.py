__author__ = 'gaugendre'
# This file is used to test solving methods from the solver module.

from solver_0hh1 import Grid

if __name__ == "__main__":
    # Doesn't need any 'two rows are not the same'
    array = [['B', ' ', ' ', ' '],
             ['B', 'B', ' ', ' '],
             [' ', ' ', ' ', ' '],
             [' ', 'B', ' ', 'B']]
    grid = Grid(len(array), array)
    print(grid)
    grid.solve()
    print(grid)

    # This one does
    array = [['B', 'B', ' ', ' ', ' ', ' '],
             [' ', ' ', ' ', ' ', ' ', 'B'],
             ['B', 'B', ' ', 'B', ' ', ' '],
             ['B', ' ', ' ', ' ', ' ', ' '],
             [' ', ' ', ' ', 'R', ' ', ' '],
             [' ', ' ', 'R', ' ', ' ', ' ']]
    grid = Grid(len(array), array)
    print(grid)
    grid.solve()
    print(grid)